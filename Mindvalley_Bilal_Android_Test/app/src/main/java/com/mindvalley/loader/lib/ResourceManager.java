package com.mindvalley.loader.lib;

import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;

import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ahmad Bilal Khan on 8/19/2016.
 */
public class ResourceManager {
    private static final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

    private static final int maxCapacity = maxMemory / 8;

    private static ResourceManager instance = new ResourceManager();

    private final LruCache<String, ResponseData> cache = new LruCache<String, ResponseData>(maxCapacity) {
        @Override
        protected int sizeOf(String key, ResponseData value) {
            return value.size();
        }
    };

    private final HashMap<String, InternalTask> runningTasks = new HashMap<>();

    public static ResourceManager getInstance() {
        return instance;
    }

    private ResourceManager() {
    }

    public ResourceTask load(String url, final Callback callback) {
        ResponseData object = cache.get(url);
        if(object != null) {
            if (callback != null) {
                callback.onResponseReceived(true, object);
            }
            return null;
        }

        InternalTask task = null;

        if(!runningTasks.containsKey(url)) {
            task = new InternalTask(url, new InternalCB() {
                @Override
                public void onCompletion(InternalTask theTask) {
                    remove(theTask.getPath());

                    if(theTask.isSuccess()) {
                        cache.put(theTask.getPath(), theTask.getResponse());
                    }
                }
            });
            task.execute();
            runningTasks.put(url, task);
        }
        else {
            task = runningTasks.get(url);
        }

        ResourceTask resourceTask = new ResourceTask(callback, task);
        task.addTask(resourceTask);
        return resourceTask;
    }

    public void cancel(ResourceTask resourceTask) {
        InternalTask task = runningTasks.get(resourceTask.getTask().getPath());
        if (task != null) {
            task.removeTask(resourceTask);
            resourceTask.nullify();
            if(!task.hasTask()) {
                remove(resourceTask.getTask().getPath());
                task.destroy();
            }
        }
    }

    public void evictAll() {
        cache.evictAll();
    }

    public void stopAll() {
        for (String url:runningTasks.keySet()) {
            runningTasks.get(url).destroy();
        }
        runningTasks.clear();
    }

    public void reset() {
        stopAll();
        evictAll();
    }

    private void remove(String url) {
        runningTasks.remove(url);
    }

    public interface Callback {
        void onResponseReceived(boolean success, ResponseData data);
    }

    protected interface InternalCB {
        void onCompletion(InternalTask theTask);
    }
}

class InternalTask extends AsyncTask<Void, Void, Boolean> {
    private String path;
    private ResourceManager.InternalCB callback;
    private ArrayList<ResourceTask> callbackTasks = new ArrayList<>();
    private ResponseData response;

    public InternalTask(String path, ResourceManager.InternalCB callback) {
        this.path = path;
        this.callback = callback;
    }

    public String getPath() {
        return path;
    }


    public ResponseData getResponse() {
        return response;
    }

    public boolean hasTask() {
        return callbackTasks.size() > 0;
    }

    public boolean isSuccess() {
        return response != null;
    }

    public void addTask(ResourceTask task) {
        callbackTasks.add(task);
    }

    public void removeTask(ResourceTask task) {
        callbackTasks.remove(task);
        if(!hasTask()) {
            cancel(true);
        }
    }

    public void executeCallback() {
        for (ResourceTask task:callbackTasks) {
            task.markDone();
            task.getCallback().onResponseReceived(isSuccess(), response);
        }
    }

    public void destroy() {
        callback = null;
        for (ResourceTask task:
             callbackTasks) {
            task.nullify();
        }
        callbackTasks.clear();
        cancel(true);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            URL url = new URL(path);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setConnectTimeout(1000 * 10);
//            connection.setReadTimeout(1000 * 10);

//            String response = Utils.readString(url.openStream(), "UTF-8");

            if(connection.getContentType().startsWith("image")) {
                response = new ResponseData(BitmapFactory.decodeStream(connection.getInputStream()), ResponseData.Type.IMAGE);
            }
            else {
                String data = Utils.readString(connection.getInputStream(), "UTF-8");

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject("{\"response\":" + data + "}");
                } catch (Exception e){
                    e.printStackTrace();
                }

                if(connection.getContentType().endsWith("json") || jsonObject != null) {
                    response = new ResponseData(jsonObject, ResponseData.Type.JSON);
                }
                else {
                    response = new ResponseData(data, ResponseData.Type.STRING);
                }
            }

            if (!isCancelled())
                return true;

            response = null;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if(callback != null) {
            callback.onCompletion(this);
            executeCallback();
            destroy();
        }
    }

    @Override
    protected void onCancelled(Boolean aBoolean) {
    }
}