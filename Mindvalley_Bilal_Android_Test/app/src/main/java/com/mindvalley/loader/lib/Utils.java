package com.mindvalley.loader.lib;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Utils {
    public static byte[] readBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];
        while (true) {
            int len = inputStream.read(buffer);
            if (len == -1) {
                break;
            }
            baos.write(buffer, 0, len);
        }
        return baos.toByteArray();
    }

    public static String readString(InputStream inputStream, String charset)
            throws IOException {
        return new String(readBytes(inputStream), charset);
    }
}
