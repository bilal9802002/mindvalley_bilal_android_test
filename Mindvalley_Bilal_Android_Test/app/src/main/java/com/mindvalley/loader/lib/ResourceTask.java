package com.mindvalley.loader.lib;

/**
 * Created by Ahmad Bilal Khan on 8/20/2016.
 */
public class ResourceTask {
    private InternalTask task;
    private ResourceManager.Callback callback;
    private boolean done = false;

    protected InternalTask getTask() {
        return task;
    }

    protected ResourceManager.Callback getCallback() {
        return callback;
    }

    protected void markDone() {
        done = true;
        nullify();
    }

    protected void nullify() {
        task = null;
    }

    protected ResourceTask(ResourceManager.Callback callback, InternalTask task) {
        this.callback = callback;
        this.task = task;
    }

    public boolean isDone() {
        return done;
    }
}
