package com.mindvalley.loader.lib;

import android.graphics.Bitmap;

import org.json.JSONObject;

/**
 * Created by Ahmad Bilal Khan on 8/19/2016.
 */
public class ResponseData {
    public enum Type {
        IMAGE,
        JSON,
        STRING
    }

    Object data;
    Type type;

    public ResponseData(Object data, Type type) {
        this.data = data;
        this.type = type;
    }

    public Object getData() {
        return data;
    }

    public Type getType() {
        return type;
    }

    public int size() {
        if(type == Type.IMAGE) {
            return ((Bitmap)data).getByteCount() / 1024 + 10;
        }
        else if(type == Type.JSON) {
            return ((JSONObject)data).length() + 10;
        }
        return ((String) data).length() + 10;
    }
}
